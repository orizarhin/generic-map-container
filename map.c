#include "map.h"
#include <stdlib.h>
#include <stdbool.h>
#define EMPTY_RESULT -1
#define EMPTY_MAP_RESULT -1
#define PREVIOUS 1
#define CURRENT 0
#define ZERO_ITERATIONS 0

//Note that both data and key are allocated by the given 'copy' functions in 'mapPut'
typedef struct nodetype {
    struct nodetype* next;
    MapDataElement data;
    MapKeyElement key;
} *Node;

//Map_t pointer type defined as 'Map' in map.h
struct Map_t {
    Node first;
    Node iterator;
    int size;
    copyMapDataElements copyData;
    copyMapKeyElements copyKey;
    freeMapDataElements freeData;
    freeMapKeyElements freeKey;
    compareMapKeyElements compareKey;
};

//Node allocation and value instantiation.
Node nodeCreate() {
    Node node = malloc(sizeof(*node));
    if (node == NULL) {
        return NULL;
    }
    node->next=NULL;
    node->data=NULL;
    node->key=NULL;
    return node;
}

//NOTE - data and key are allocated in 'mapPut'
void nodeDestroy(Map map, Node node) {
    map->freeData(node->data);
    map->freeKey(node->key);
    free(node);
}


Map mapCreate(copyMapDataElements copyDataElement,
              copyMapKeyElements copyKeyElement,
              freeMapDataElements freeDataElement,
              freeMapKeyElements freeKeyElement,
              compareMapKeyElements compareKeyElements) {
    if (!copyDataElement || !copyKeyElement || !freeDataElement || !freeKeyElement || !compareKeyElements) {
        return NULL;
    }
    Map map = malloc(sizeof(*map));
    if (map==NULL) {
        return NULL;
    }
    map->copyData = copyDataElement;
    map->copyKey = copyKeyElement;
    map->freeData = freeDataElement;
    map->freeKey = freeKeyElement;
    map->compareKey = compareKeyElements;
    map->first = NULL;
    map->iterator = NULL;
    map->size = 0;
    return map;
}


/*
 * destroys all nodes connected to the one sent, including that one.
 * will also destroy all the data and keys connected, which are allocated in 'mapPut' func.
 */
void destroyNodeChain(Map map, Node node) {
    if (map == NULL) {
        return;
    }
    Node temp1=node;
    while(temp1!=NULL) {
        Node temp2=temp1;
        temp1 = temp1->next;
        map->freeKey(temp2->key);
        map->freeData(temp2->data);
        free(temp2);
    }
}

//de-allocates all memory allocated internally.
void mapDestroy(Map map) {
    if (map==NULL) {
        return;
    }
    destroyNodeChain(map, map->first);
    free(map);
}

/* returns an exact copy of sent node, allocating and copying both data and key.
 * while the 'next' object points to given node's following node, it's important to note that the
 * given node's father will point towards the original node, and not to this one.
 */
Node nodeCopy(Map map, Node node) {
    if (node == NULL) {
        return NULL;
    }
    Node tempnode = nodeCreate();
    tempnode->next = node->next;
    tempnode->data = map->copyData(node->data);
    tempnode->key = map->copyKey(node->key);
    return tempnode;
}

//returns an exact copy of sent map, allocating and copying all its elements.
Map mapCopy(Map map) {
    if (map == NULL) {
        return NULL;
    }
    Map newmap = mapCreate(map->copyData, map->copyKey,
                            map->freeData, map->freeKey,
                            map->compareKey);
    if (newmap == NULL) {
        return NULL;
    } //can't return MAP_OUT_OF_MEMORY (according to TA's instructions).
    map->iterator=NULL; //requested
    if(map->first==NULL) {
        return newmap;
    }
    newmap->size = map->size;
    Node node_iterator = map->first;
    newmap->first = nodeCopy(map, node_iterator);
    node_iterator = node_iterator->next;
    Node new_node_iterator = newmap->first;
    while (node_iterator!=NULL) {
        Node tempnode = nodeCopy(map, node_iterator);
        new_node_iterator->next = tempnode;
        new_node_iterator = new_node_iterator->next;
        node_iterator = node_iterator->next;
    }
    return newmap;
}

int mapGetSize(Map map) {
    if (map==NULL) {
        return EMPTY_MAP_RESULT;
    }
    return map->size;
}


/*
 * mode == CURRENT:
 * returns NULL (and doesn't change compare_result) if:
 * - input is null
 * - map->first is null
 * if key isn't found returns the last node with a 'smaller' key
 * - ***if first node already has a larger key it will return the first node***
 * returns Node if found
 * returns difference via pointer
 *
 * mode == PREVIOUS:
 * if found return the node before.
 * -  unless it is the first node, in which case that will be the one returned,
 *    use compare_result as a differentiate.
 *
 * i: iterator - keeps track on which node the search has halted.
 */
Node findNode(Map map, MapKeyElement element, int* compare_result, int mode, int* i) {
    if (map == NULL || element == NULL) {
        return NULL;
    }
    Node node_iterator = map->first;
    Node previous_node = node_iterator;
    for (*i = ZERO_ITERATIONS ; node_iterator != NULL ; *i=*i+1) {
        *compare_result = map->compareKey(element, node_iterator->key);
        if (*compare_result < 0) {
            break;
        } else if (*compare_result == 0) {
            if (mode == CURRENT) return node_iterator;
            else if (mode == PREVIOUS) return previous_node;
        }
        previous_node=node_iterator;
        node_iterator=node_iterator->next;
    }
    return previous_node;
}


bool mapContains(Map map, MapKeyElement element) {
    int i=ZERO_ITERATIONS, compare_result = EMPTY_RESULT; //-1 to take care of case map==NULL || element==NULL
    findNode(map, element, &compare_result, CURRENT, &i);
    if(compare_result==0) {
        return true;
    }
    return false;
}


MapResult mapPut(Map map, MapKeyElement keyElement, MapDataElement dataElement) {
    if (map == NULL || keyElement == NULL || dataElement == NULL) {
        return MAP_NULL_ARGUMENT;
    }
    map->iterator=NULL; //requested
    int compare_result=EMPTY_RESULT, i=ZERO_ITERATIONS;
    Node found_node = findNode(map, keyElement,&compare_result, CURRENT, &i);
    if (found_node == NULL) { //case - first node
        found_node = nodeCreate();
        if (found_node == NULL) {
            return MAP_OUT_OF_MEMORY;
        }
        found_node->key=map->copyKey(keyElement);
        found_node->data=map->copyData(dataElement);
        map->first=found_node;
        map->size++;
    } else if (compare_result == 0) { //case - change data
        map->freeData(found_node->data);
        found_node->data=map->copyData(dataElement);
    } else { //case - add a new node after found_node
        Node temp_node = nodeCreate();
        if (temp_node == NULL) {
            return MAP_OUT_OF_MEMORY;
        }
        temp_node->key=map->copyKey(keyElement); //allocation
        temp_node->data=map->copyData(dataElement); //allocation
        if (found_node == map->first && i==0) { //case - first node returned: put before first
            temp_node->next=found_node;
            map->first=temp_node;
        } else {
            temp_node->next=found_node->next;
            found_node->next=temp_node;
        }
        map->size++;
    }
    return MAP_SUCCESS;
}


MapDataElement mapGet(Map map, MapKeyElement keyElement) { //returns the data
    if (map == NULL || keyElement == NULL) {
        return NULL;
    }
    int compare_result = EMPTY_RESULT, i=ZERO_ITERATIONS;
    Node found_node = findNode(map, keyElement, &compare_result, CURRENT, &i);
    if (compare_result == 0) {
        return found_node->data;
    }
    return NULL;
}


MapResult mapRemove(Map map, MapKeyElement keyElement) {
    if (map == NULL || keyElement == NULL) {
        return MAP_NULL_ARGUMENT;
    }
    map->iterator=map->first;
    Node temp_node;
    if (map->compareKey(map->first->key, keyElement) == 0) { //case - first node
        temp_node = map->first;
        map->first=map->first->next;
    } else { //case - any other node
        int compare_result = EMPTY_RESULT, i=ZERO_ITERATIONS;
        Node found_node = findNode(map, keyElement, &compare_result, PREVIOUS, &i);
        if (compare_result != 0) {
            return MAP_ITEM_DOES_NOT_EXIST;
        }
        temp_node = found_node->next;
        found_node->next=temp_node->next;
    }
    nodeDestroy(map, temp_node);
    map->size--;
    return MAP_SUCCESS;
}


MapKeyElement mapGetFirst(Map map) {
    if (map == NULL || map->first == NULL) {
        return NULL;
    }
    map->iterator=map->first;
    return map->first->key;
}


MapKeyElement mapGetNext(Map map) {
    if (map == NULL || map->iterator == NULL || map->iterator->next == NULL) {
        return NULL;
    }
    map->iterator=map->iterator->next;
    return map->iterator->key;
}


MapResult mapClear(Map map) {
    if (map == NULL) {
        return MAP_NULL_ARGUMENT;
    }
    destroyNodeChain(map, map->first);
    return MAP_SUCCESS;
}
